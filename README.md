# ArgoCD

## Connect to the AKS  & Do all the commands later in the AKS
~~~
az account set --subscription <subscription ID>
az aks get-credentials --resource-group <RESOURCE_GROUP_NAME> --name <AKS_CLUSTER_NAME>
~~~

## creates a Kubernetes namespace called "cicd"
~~~
kubectl create namespace cicd
~~~

## Deploy ArgoCD in the cicd namespace:
~~~
kubectl apply -n cicd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
~~~

## Get a password tor ArgoCD
~~~
kubectl -n cicd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d
~~~

## Change the argocd-server service type to LoadBalancer
~~~
kubectl patch svc argocd-server -n cicd -p '{"spec": {"type": "LoadBalancer"}}'\
~~~

## list all the services in the "cicd" namespace
~~~
kubectl get svc -n cicd
~~~

## Connect to ArgoCD
~~~
kubectl port-forward -n cicd service/argocd-server 9090:80
http://localhost:80

user:admin/password- the output of the command:

kubectl -n cicd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d\
~~~

## Manual removal of ArgoCD from the cicd namespace:
~~~
kubectl delete -n cicd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

kubectl delete namespace cicd
~~~